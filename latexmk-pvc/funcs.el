(defun latexmk-pvc ()
  "runs $latexmk -pvc current_file.tex &"
  (interactive)
  (save-window-excursion
    (async-shell-command (format "latexmk -pvc %s" buffer-file-name)))
  (message "running latexmk -pvc in background")
  )
